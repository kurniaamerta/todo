Rails.application.routes.draw do
  devise_for :users
  resources :activities do
    member do 
      put '/update_status' => 'activities#update_status'
    end
  end
  # get 'home/index'
  root 'home#index'
  get '/about', to: 'home#about',  as: 'about'
  put '/update_status_inprogress/:id', to: 'activities#update_status_inprogress',  as: 'update_status_inprogress'
  put '/update_status_done/:id', to: 'activities#update_status_done',  as: 'update_status_done'
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
end
