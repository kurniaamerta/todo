json.extract! activity, :id, :type_activity, :title, :description, :created_at, :updated_at
json.url activity_url(activity, format: :json)
