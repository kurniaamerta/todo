document.addEventListener('turbo:load', function() {
    $( "[id='Backlog'], [id='In progress'], [id='Done']" ).sortable({
        connectWith: ".connectedSortable",
        update:  function (event, ui) {
            let data = $(this).sortable('toArray');

            for(var i =0; i<data.length; i++){
                $.ajax({
                    method: "PUT",
                    url: "/activities/"+data[i]+"/update_status",
                    data: { type_activity: event.target.id , id: data[i] }
                }).fail(function( jqXHR, textStatus ) {
                    console.log( "Request failed: " + textStatus );
                });
            }
        }
    });
})