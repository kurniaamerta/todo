# frozen_string_literal: true

class NotifComponent < ViewComponent::Base
  def initialize(notice: "")
    @notice = notice
  end

end
