# frozen_string_literal: true

class CardTodoListComponent < ViewComponent::Base
  def initialize(activities:, progres_type:, background_color:)
    @activities = activities
    @progres_type = progres_type
    @background_color = background_color
  end

end
